     Установка
1. Создайте БД с именем `uvent`, доступную для пользователя `root` без пароля
2. `composer install`
3. `yii migrate/up`

> Методы API

    Работа с пользователями:

- Авторизация `POST api/user/sign-in`
- Регистрация `POST api/user/sign-up`
- Информация об авторизованном пользователе `GET api/user/me`
- Закрытие сессии (удаление `access_token`) `POST api/user/sign-out`

    
    Работа с событиями (требуется <access_token>)
    
    
- Список всех событий `GET api/event/list`
- Удаление события (владельцем) `DELETE api/event/{id}/delete`
- Подписка на событие `POST api/event/{id}`
- Отписка от события `DELETE api/event/{id}`
- Создание события `POST api/event/create`


> Об API

Работать с "бизнес-логикой" API можно лишь имея `access_token`, который генерируется пользователю после успешной авторизации.

Полученный `access_token` необходимо передавать при каждом запросе к API в заголовке `Authorization` => `Bearer  <ваш access_token>`
