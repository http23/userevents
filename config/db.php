<?php

use yii\db\Connection;

return [
    'class' => Connection::class,
    'dsn' => 'mysql:host=localhost;dbname=uvent',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
