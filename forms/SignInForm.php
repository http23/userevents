<?php

namespace app\forms;


use app\models\User;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Model;

/**
 * @property-read User $user
 */
class SignInForm extends Model
{
    public $login;
    public $password;

    /**
     * @var User
     */
    private $_user;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['password', 'login'], 'required'],
        ];
    }

    /**
     * @return bool
     * @throws InvalidArgumentException
     */
    public function isValid(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findOne(['login' => $this->login]);

        if ($user === null || !Yii::$app->security->validatePassword($this->password, $user->password)) {
            $this->addError('login', 'Логин или пароль введен не верно');

            return false;
        }

        $this->_user = $user;

        return true;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->_user;
    }
}
