<?php

namespace app\controllers\rest;

use app\controllers\rest\actions\event\CreateAction;
use app\controllers\rest\actions\event\DeleteAction;
use app\controllers\rest\actions\event\ListAction;
use app\controllers\rest\actions\event\SubscribeToEventAction;
use app\controllers\rest\actions\event\UnSubscribeFromEventAction;
use app\models\Event;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

class EventController extends Controller
{
    private $_modelClass = Event::class;

    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['list', 'create', 'subscribe', 'unsubscribe', 'delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => ['POST'],
                    'subscribe' => ['POST'],
                    'unsubscribe' => ['DELETE'],
                    'delete' => ['DELETE'],
                    '*' => ['GET', 'HEAD'],
                ],
            ],
        ], parent::behaviors());
    }

    public function actions()
    {
        return [
            'subscribe' => [
                'class' => SubscribeToEventAction::class,
                'modelClass' => $this->_modelClass,
            ],
            'unsubscribe' => [
                'class' => UnSubscribeFromEventAction::class,
                'modelClass' => $this->_modelClass,
            ],
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->_modelClass,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => $this->_modelClass,
            ],
            'list' => [
                'class' => ListAction::class,
                'modelClass' => $this->_modelClass,
            ],
        ];
    }
}
