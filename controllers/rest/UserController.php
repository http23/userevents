<?php

namespace app\controllers\rest;

use app\controllers\rest\actions\user\MeAction;
use app\controllers\rest\actions\user\SignInAction;
use app\controllers\rest\actions\user\SignOutAction;
use app\controllers\rest\actions\user\SignUpAction;
use app\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

class UserController extends Controller
{
    private $_modelClass = User::class;

    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['sign-in', 'sign-up'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['me', 'sign-out'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'sign-in' => ['POST'],
                    'sign-up' => ['POST'],
                    'sign-out' => ['POST'],
                    '*' => ['GET', 'HEAD'],
                ],
            ],
        ], parent::behaviors());
    }

    public function actions()
    {
        return [
            'sign-up' => [
                'class' => SignUpAction::class,
                'modelClass' => $this->_modelClass,
            ],
            'sign-in' => [
                'class' => SignInAction::class,
                'modelClass' => $this->_modelClass,
            ],
            'sign-out' => [
                'class' => SignOutAction::class,
                'modelClass' => $this->_modelClass,
            ],
            'me' => [
                'class' => MeAction::class,
                'modelClass' => $this->_modelClass,
            ],
        ];
    }
}
