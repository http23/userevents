<?php

namespace app\controllers\rest\actions\user;

use app\models\User;
use Yii;
use yii\rest\Action;

class MeAction extends Action
{
    public function run(): array
    {
        /** @var User $me */
        $me = Yii::$app->user->getIdentity();

        return $me->toArray([], ['birth_at', 'memberEvents']);
    }
}
