<?php

namespace app\controllers\rest\actions\user;

use app\models\User;
use Yii;
use yii\rest\Action;

class SignOutAction extends Action
{
    public function run()
    {
        /** @var User $user */
        $user = Yii::$app->user->getIdentity();

        $user->access_token = null;
        $user->save(true, ['access_token']);

        Yii::$app->user->logout();

        Yii::$app->response->statusCode = 204;
    }
}
