<?php

namespace app\controllers\rest\actions\user;

use app\forms\SignInForm;
use app\models\User;
use Yii;
use yii\base\Exception;
use yii\rest\Action;

class SignInAction extends Action
{
    /**
     * @return User|array
     * @throws Exception
     */
    public function run()
    {
        $loginForm = new SignInForm();

        $loginForm->load(Yii::$app->request->post(), '');

        if (!$loginForm->validate() || !$loginForm->isValid()) {
            return ['errors' => $loginForm->errors];
        }

        $loginForm->user->updateToken();

        return $loginForm->user->toArray([], ['access_token', 'birth_at']);
    }
}
