<?php

namespace app\controllers\rest\actions\user;

use app\models\User;
use Yii;
use yii\base\Exception;
use yii\rest\Action;

class SignUpAction extends Action
{
    /**
     * @return array
     * @throws Exception
     */
    public function run(): array
    {
        $user = new User();
        $user->load(Yii::$app->request->post(), '');

        if ($password = Yii::$app->request->post('password')) {
            $user->password = Yii::$app->security->generatePasswordHash($password);
        }

        if (!$user->save()) {
            return ['errors' => $user->errors];
        }

        return ['user' => $user];
    }
}
