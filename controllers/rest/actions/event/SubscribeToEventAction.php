<?php

namespace app\controllers\rest\actions\event;

use app\models\Event;
use app\models\User;
use app\models\UserToEvent;
use Yii;
use yii\base\ErrorException;
use yii\rest\Action;

class SubscribeToEventAction extends Action
{
    public function run($id)
    {
        /** @var Event $event */
        $event = $this->findModel($id);

        /** @var User $user */
        $user = Yii::$app->user->getIdentity();

        if (UserToEvent::find()->where(['user_id' => $user->id, 'event_id' => $event->id])->exists()) {
            throw new ErrorException('Вы уже участвуете в этом событии');
        }

        try {
            $event->link('users', $user);
        } catch (\Exception $exception) {
            throw new ErrorException($exception->getMessage());
        }

        return ['event' => $event];
    }
}
