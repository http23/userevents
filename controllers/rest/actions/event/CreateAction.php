<?php

namespace app\controllers\rest\actions\event;

use app\models\Event;
use Yii;
use yii\rest\Action;

class CreateAction extends Action
{
    public function run()
    {
        $event = new Event();

        $event->load(Yii::$app->request->post(), '');

        if ($event->save()) {
            $event->refresh();

            return ['event' => $event];
        }

        return ['errors' => $event->errors];
    }
}
