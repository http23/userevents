<?php

namespace app\controllers\rest\actions\event;

use app\models\Event;
use app\models\User;
use Yii;
use yii\rest\Action;
use yii\web\MethodNotAllowedHttpException;

class DeleteAction extends Action
{
    public function run($id)
    {
        /** @var Event $event */
        $event = $this->findModel($id);

        /** @var User $user */
        $user = Yii::$app->user->getIdentity();

        if ($event->created_by !== $user->id) {
            throw new MethodNotAllowedHttpException('Нельзя удалить чужое событие');
        }

        $event->delete();

        Yii::$app->response->statusCode = 204;
    }
}
