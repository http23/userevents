<?php

namespace app\controllers\rest\actions\event;

use app\models\Event;
use yii\data\ActiveDataProvider;
use yii\rest\Action;

class ListAction extends Action
{
    public function run()
    {
        return new ActiveDataProvider([
            'query' => Event::find()->with(['owner', 'users']),
        ]);
    }
}
