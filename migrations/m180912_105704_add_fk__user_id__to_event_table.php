<?php

use yii\db\Migration;

/**
 * Class m180912_105704_add_fk__user_id__to_event_table
 */
class m180912_105704_add_fk__user_id__to_event_table extends Migration
{
    const FK_NAME = 'fk_$created_by__$id__user';

    const USER_TN = 'user';
    const EVENT_TN = 'event';

    public function safeUp()
    {
        $this->addForeignKey(
            self::FK_NAME,
            self::EVENT_TN,
            'created_by',
            self::USER_TN,
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_NAME, self::EVENT_TN);
    }
}
