<?php

use yii\db\Migration;

/**
 * Handles the creation of table `event`.
 */
class m180912_104920_create_event_table extends Migration
{
    const TABLE_NAME = 'event';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->unsigned()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
