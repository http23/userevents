<?php

use yii\db\Migration;

/**
 * Class m180912_110057_add_fk__event_id__to_user_to_event_table
 */
class m180912_110057_add_fk__event_id__to_user_to_event_table extends Migration
{
    const FK_NAME = 'fk_$event_id__$id__event';

    const EVENT_TN = 'event';
    const USER_TO_EVENT_TN = 'user_to_event';

    public function safeUp()
    {
        $this->addForeignKey(
            self::FK_NAME,
            self::USER_TO_EVENT_TN,
            'event_id',
            self::EVENT_TN,
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_NAME, self::USER_TO_EVENT_TN);
    }
}
