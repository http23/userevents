<?php

use yii\db\Migration;

/**
 * Class m180912_105949_add_fk__user_id__to_user_to_event_table
 */
class m180912_105949_add_fk__user_id__to_user_to_event_table extends Migration
{
    const FK_NAME = 'fk_$user_id__$id__user';

    const USER_TN = 'user';
    const USER_TO_EVENT_TN = 'user_to_event';

    public function safeUp()
    {
        $this->addForeignKey(
            self::FK_NAME,
            self::USER_TO_EVENT_TN,
            'user_id',
            self::USER_TN,
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_NAME, self::USER_TO_EVENT_TN);
    }
}
