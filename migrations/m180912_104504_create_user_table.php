<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180912_104504_create_user_table extends Migration
{
    const TABLE_NAME = 'user';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->unsigned(),
            'login' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'birth_at' => $this->dateTime(),
            'access_token' => $this->string(32),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
