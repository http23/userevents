<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_to_event`.
 */
class m180912_105055_create_user_to_event_table extends Migration
{
    const TABLE_NAME = 'user_to_event';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'event_id' => $this->integer()->unsigned()->notNull(),
        ]);

        $this->createIndex(
            'ui__user_id__event_id',
            self::TABLE_NAME,
            ['user_id', 'event_id'],
            true
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
