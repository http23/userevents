<?php

namespace app\models;

use DateTime;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "event".
 *
 * @property int           $id
 * @property string        $title
 * @property string        $description
 * @property string        $created_at
 * @property int           $created_by
 *
 * @property User          $owner
 * @property User[]        $users
 * @property UserToEvent[] $userToEvents
 */
class Event extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'event';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['title', 'description'], 'required'],
            [['description'], 'string'],
            [['created_at'], 'safe'],
            [['created_by'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => false,
            ],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'created_at' => 'Дата создания',
            'created_by' => 'Создатель',
        ];
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'description',
            'created_at' => function (self $model) {
                return (new DateTime($model->created_at))->format('d.m.Y H:i:s');
            },
            'owner',
            'users',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOwner(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserToEvents(): ActiveQuery
    {
        return $this->hasMany(UserToEvent::class, ['event_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUsers(): ActiveQuery
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->via('userToEvents');
    }
}
