<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int           $id
 * @property string        $login
 * @property string        $password
 * @property string        $first_name
 * @property string        $last_name
 * @property string        $created_at
 * @property string        $birth_at
 * @property string        $access_token
 *
 * @property Event[]       $ownedEvents  - Массив событий, владельцем которых являюсь я
 * @property Event[]       $memberEvents - Массив событий, в которых я участвую
 * @property UserToEvent[] $userToEvents - Вспомогательный релейшен
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @param int|string $id
     * @return null|self
     */
    public static function findIdentity($id): ?self
    {
        return static::findOne($id);
    }

    /**
     * @param mixed $token
     * @param null  $type
     * @return self|null
     */
    public static function findIdentityByAccessToken($token, $type = null): ?self
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'user';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthKey(): string
    {
        return $this->access_token;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->access_token === $authKey;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['login', 'password', 'first_name', 'last_name'], 'required'],
            [['created_at', 'birth_at', '!password'], 'safe'],
            [['login'], 'unique'],
            [['access_token'], 'string', 'max' => 32],
            [['login', 'password', 'first_name', 'last_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ]);
    }

    public function fields()
    {
        return [
            'id',
            'login',
            'first_name',
            'last_name',
        ];
    }

    public function extraFields()
    {
        return ['access_token', 'birth_at', 'memberEvents'];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'created_at' => 'Дата регистрации',
            'birth_at' => 'Дата рождения',
            'access_token' => 'API key',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOwnedEvents(): ActiveQuery
    {
        return $this->hasMany(Event::class, ['created_by' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserToEvents(): ActiveQuery
    {
        return $this->hasMany(UserToEvent::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMemberEvents(): ActiveQuery
    {
        return $this->hasMany(Event::class, ['id' => 'event_id'])->via('userToEvents');
    }

    public function updateToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();

        $this->save(true, ['access_token']);
    }
}
