<?php

namespace app\components;


use app\models\User;
use Yii;
use yii\base\Action;
use yii\base\Module;
use yii\helpers\StringHelper;

class Api extends Module
{
    public function init()
    {
        $this->controllerNamespace = 'app\controllers\rest';

        Yii::$app->user->enableSession = false;
        Yii::$app->user->loginUrl = null;

        parent::init();
    }

    /**
     * @param Action $action
     * @return bool
     */
    public function beforeAction($action): bool
    {
        $this->validateAuthorizationHeader();

        return parent::beforeAction($action);
    }

    private function validateAuthorizationHeader(): bool
    {
        $authType = 'Bearer ';
        $authData = Yii::$app->request->headers->get('Authorization');

        if (!StringHelper::startsWith($authData, $authType)) {
            return false;
        }

        $token = strtr($authData, [$authType => '']);

        $user = User::findIdentityByAccessToken($token);

        if (null === $user) {
            return false;
        }

        Yii::$app->user->login($user);

        return true;
    }
}
