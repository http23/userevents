var cookieHelper,
    app,
    selectedEvent;

cookieHelper = {
    'getCookie': function (name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));

        return matches ? decodeURIComponent(matches[1]) : undefined;
    },
    'setCookie': function (key, value) {
        document.cookie = key + '=' + value;
    },
    'removeCookie': function (key) {
        this.setCookie(key, '; expires=-1; Max-Age=0');
    }
};

app = {
    'isLogedIn': function () {
        return (typeof cookieHelper.getCookie('credentials') !== 'undefined');
    },
    'signOut': function () {
        this.createRequest('/api/user/sign-out', null, 'POST').done(function (data) {
            cookieHelper.removeCookie('credentials');

            app.bootstrap();
        });
    },
    'loadPage': function (page) {
        this.createRequest('/static/' + page + '.html', null, 'GET', 'html').done(function (data) {
            window.history.pushState('', '', '/#' + page);

            $('#content').html(data);

            $('#content .contentable').each(function () {
                var contentKey = $(this).attr('data-content'),
                    content = eval(contentKey);

                $(this).html(content);
            });
        });
    },
    'getCredentials': function () {
        return JSON.parse(cookieHelper.getCookie('credentials'));
    },
    'bootstrap': function () {
        if (this.isLogedIn()) {
            this.loadPage('index');
        } else {
            this.loadPage('default');
        }
    },
    'createRequest': function (url, params, method, dataType) {
        var requestParams = {
            url: url,
            type: method ? method : 'GET',
            data: params,
            dataType: dataType ? dataType : 'json',
            beforeSend: function (xhr) {
                xhr.overrideMimeType("text/plain; charset=utf8");
            }
        };

        if (this.isLogedIn()) {
            var accessToken = this.getCredentials().access_token;

            requestParams.beforeSend = function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken);
            };
        }

        return $.ajax(requestParams);
    },
    'authByPassword': function (params) {
        this.createRequest('/api/user/sign-in', params, 'POST').done(function (data) {
            if (data.access_token) {
                cookieHelper.setCookie('credentials', JSON.stringify(data));

                app.loadPage('index');
            } else if (data.errors) {
                var errors = [];

                for (var i in data.errors) {
                    errors.push(data.errors[i][0]);
                }

                alert(errors.join("\n"));
            }
        });
    },
    'updateSelectedEvent': function () {
        if (!selectedEvent) {
            return false;
        }

        setTimeout(function () {
            $('.all-events-html div[data-id="' + selectedEvent + '"]').click();
        }, 500, selectedEvent);
    },
    'renderAllEventsHtml': function () {
        this.createRequest('/api/event/list').done(function (data) {
            if (!data) {
                return false;
            }

            var html = '';

            if (data.length) {
                for (var i in data) {
                    html += '<div data-id="' + data[i].id + '" data-content=\'' + JSON.stringify(data[i]) + '\'>' + data[i].title + '</div>';
                }
            } else {
                html = '<span class="empty-cell">Событий нет...</span>';
            }

            $('.all-events-html').html(html);
        });
    },
    'renderMyEventsHtml': function () {
        this.createRequest('/api/user/me').done(function (data) {
            if (!data) {
                return false;
            }

            var html = '';

            if (data.memberEvents.length) {
                for (var i in data.memberEvents) {
                    html += '<div data-id="' + data.id + '" data-content=\'' + JSON.stringify(data.memberEvents[i]) + '\'>' + data.memberEvents[i].title + '</div>';
                }
            } else {
                html = '<span class="empty-cell">Событий нет...</span>';
            }

            $('.my-events-html').html(html);
            app.updateSelectedEvent();
        });
    },
    'renderEventHtml': function (data) {
        selectedEvent = data.id;

        var imHere = false,
            html = '<h4>' + data.title + '</h4><hr>' +
                '<p>' + data.description + '</p>' +
                '<p>' + data.created_at + '</p>' +
                '<br><p><b>Участники</b></p><hr>';

        if (data.users.length) {
            html += '<div class="users-in-event">';
            for (var i in data.users) {
                html += '<div data-content=\'' + JSON.stringify(data.users[i]) + '\'>' + data.users[i].first_name + ' ' + data.users[i].last_name + '</div>';

                if (data.users[i].id === app.getCredentials().id) {
                    imHere = true;
                }
            }
            html += '</div>';
        } else {
            html += 'Отсутсвуют...';
        }

        if (imHere) {
            html += '<hr><button class="btn btn-warning unsubscribe-from-event" data-id="' + data.id + '">Отказаться от участия</button>';
        } else {
            html += '<hr><button class="btn btn-success subscribe-to-event" data-id="' + data.id + '">Принять участие</button>';
        }

        $('.event-item').html(html);
    }
};

$(document).ready(function () {
    app.bootstrap();

    $('body').on('click', '.linkable', function () {
        app.loadPage($(this).attr('data-href'));

        return false;
    });

    $('body').on('click', '.sign-out', function () {
        app.signOut();
    });

    $(window).bind('hashchange', function (e) {
        var dataPage = e.originalEvent.newURL.split('#');

        if (dataPage[1]) {
            app.loadPage(dataPage[1]);
        }
    });
});
